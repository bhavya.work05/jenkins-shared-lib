
def call() {
    // Use withCredentials to access Docker Hub credentials
    withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', usernameVariable: 'DOCKER_USER', passwordVariable: 'DOCKER_PASS')]) {
        sh "docker build -t bhavyaa55/java-maven-app:jma-3.0 ."
        sh "echo $DOCKER_PASS | docker login -u $DOCKER_USER --password-stdin"
        sh "docker push bhavyaa55/java-maven-app:jma-3.0"

    }
}